import { useRef } from 'react';

const useArrayRef = (array) => {
  const refs = useRef([]);

  if (refs.current.length !== array.length) {
    refs.current = Array.from({ length: array.length });
  }

  const addRef = (index) => (ref) => (refs.current[index] = ref);

  return { refs, addRef };
};

export default useArrayRef;
