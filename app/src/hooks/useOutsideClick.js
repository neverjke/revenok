import { useRef } from 'react';
import useEventListener from '@/hooks/useEventListener';

const useOutsideClick = (callback) => {
  const ref = useRef(null);

  useEventListener('click', (event) => {
    if (!event.composedPath().includes(ref.current)) {
      callback();
    }
  });

  return ref;
};

export default useOutsideClick;
