import { useEffect, useLayoutEffect } from 'react';

const useEventListener = (type, handler, dependencies = [], params = {}) => {
  const {
    immediate = false,
    layout = false,
    target = window,
    listenerParams = {},
  } = params;

  const method = layout ? useLayoutEffect : useEffect;

  method(() => {
    const targetElement = target.current || target;

    targetElement.addEventListener(type, handler, listenerParams);
    if (immediate) {
      handler();
    }

    return () => {
      targetElement.removeEventListener(type, handler, listenerParams);
    };
  }, dependencies);
};

export default useEventListener;
