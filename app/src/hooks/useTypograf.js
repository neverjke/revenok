import Typograf from 'typograf';

const useTypograf = () => {
  const tp = new Typograf({
    locale: 'en-US',
    mode: 'name',
  });

  return (text) => {
    if (!text) return null;

    return tp.execute(text);
  };
};

export default useTypograf;
