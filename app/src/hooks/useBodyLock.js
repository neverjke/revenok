import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks,
} from 'body-scroll-lock';

const setPaddingCorrectors = (padding) => {
  document
    .querySelectorAll('[data-padding-corrector]')
    .forEach((paddingCorrector) => {
      paddingCorrector.style.paddingRight = `${padding}px`;
    });
};

const useBodyLock = () => {
  const lockScroll = (element) => {
    const scrollBarWidth = window.innerWidth - document.body.offsetWidth;

    setPaddingCorrectors(scrollBarWidth);

    // document.body.style.paddingRight = `${scrollBarWidth}px`;

    disableBodyScroll(element);
  };
  const unlockScroll = (element) => {
    // document.body.style.paddingRight = '0px';

    setPaddingCorrectors(0);

    enableBodyScroll(element);
  };
  const unlockAllScrolls = () => {
    // document.body.style.paddingRight = '0px';

    setPaddingCorrectors(0);

    clearAllBodyScrollLocks();
  };

  return {
    unlockScroll,
    lockScroll,
    unlockAllScrolls,
  };
};

export default useBodyLock;
