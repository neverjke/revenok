import { getViewport } from '@/tools/resize';

const setVH = () => {
  const vh = getViewport().height * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
};

export default setVH;
