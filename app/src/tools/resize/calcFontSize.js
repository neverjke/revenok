import resize from '@/config/resize';
import { checkIsMobile, getViewport } from '@/tools/resize';

const calcFontSize = () => {
  const { width, height } = getViewport();

  const config = checkIsMobile() ? resize.mobile : resize.desktop;

  const horizontalRatio =
    Math.max(config.minWidth, width) / config.defaultWidth;
  const verticalRatio =
    Math.max(config.minHeight, height) / config.defaultHeight;
  const minRatio = Math.min(horizontalRatio, verticalRatio);

  return resize.defaultFont * minRatio;
};

export default calcFontSize;
