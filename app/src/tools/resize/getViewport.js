const getViewport = () => ({
  // width: document.querySelector('#root').offsetWidth,
  // height: document.querySelector('#root').offsetHeight,
  width: window.innerWidth,
  height: window.innerHeight,
});

export default getViewport;
