import resize from '@/config/resize';
import { getViewport } from '@/tools/resize';

const checkIsMobile = () => getViewport().width < resize.desktop.minWidth;

export default checkIsMobile;
