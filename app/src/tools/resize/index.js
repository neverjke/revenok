export { default as calcFontSize } from './calcFontSize';
export { default as getViewport } from './getViewport';
export { default as setVH } from './setVH';
export { default as checkIsMobile } from './checkIsMobile';
