const em = (pixels, fontSize) => (pixels / 16) * fontSize;

export default em;
