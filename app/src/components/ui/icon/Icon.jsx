import React, { cloneElement, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

function Icon({ className = '', name = '', folder = '', onClick = () => {} }) {
  const [iconComponent, setIconComponent] = useState(null);

  useEffect(() => {
    if (!name) {
      setIconComponent(null);
      return;
    }

    const importSvg = folder
      ? import(`../../../assets/svg/${folder}/${name}.svg`)
      : import(`../../../assets/svg/${name}.svg`);

    importSvg
      .then((svg) => {
        setIconComponent(svg.ReactComponent);
      })
      .catch((error) => {
        setIconComponent(null);
        console.warn(error);
      });
  }, [name, folder]);

  if (!iconComponent) return <svg className={className} />;

  return cloneElement(iconComponent, { className, onClick });
}

Icon.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  folder: PropTypes.string,
  onClick: PropTypes.func,
};

export default Icon;
