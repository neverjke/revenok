import React from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import './button.scss';

import Text from '@/components/ui/text/Text';
import Icon from '@/components/ui/icon/Icon';

function Button({
  className = '',
  onClick = () => {},
  href = '',
  children = '',
  size = 'l',
  theme = 'transparent',
  isDisabled = false,
  icon = '',
  iconFolder = '',
}) {
  const ButtonElement = href ? 'div' : 'button';
  const iconOnly = icon && !children;

  return (
    <ButtonElement
      onClick={onClick}
      disabled={isDisabled}
      className={classNames(
        className,
        'button',
        `button--size--${size}`,
        `button--theme--${theme}`,
        {
          'button--icon-only': iconOnly,
        },
      )}
    >
      {href && (
        <a
          className="absolute-fill"
          href={href}
          target="_blank"
          rel="noreferrer"
        />
      )}
      {icon && (
        <Icon className="button__icon" name={icon} folder={iconFolder} />
      )}
      {children && <Text className="button__text" text={children} />}
    </ButtonElement>
  );
}

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  href: PropTypes.string,
  children: PropTypes.string,
  size: PropTypes.oneOf(['l']),
  theme: PropTypes.oneOf(['accent', 'accent-second', 'transparent']),
  isDisabled: PropTypes.bool,
  icon: PropTypes.string,
  iconFolder: PropTypes.string,
};

export default Button;
