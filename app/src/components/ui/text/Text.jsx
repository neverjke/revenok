import React from 'react';
import PropTypes from 'prop-types';

import useTypograf from '@/hooks/useTypograf';

function Text({
  className = '',
  text = '',
  withWrapper = true,
  tag = 'span',
  ...attributes
}) {
  const tp = useTypograf();

  const resultText = tp(text);

  return withWrapper
    ? React.createElement(tag, {
        className,
        dangerouslySetInnerHTML: { __html: resultText },
        ...attributes,
      })
    : resultText;
}

Text.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  withWrapper: PropTypes.bool,
  tag: PropTypes.string,
};

export default Text;
