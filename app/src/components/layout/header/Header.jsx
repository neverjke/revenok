import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import './header.scss';

import gsap from '@/lib/gsap';
import CustomEase from '@/lib/gsap/CustomEase';

import Icon from '@/components/ui/icon/Icon';
import Button from '@/components/ui/button/Button';
import transitions from '@/config/transitions';

const Header = forwardRef(({ className }, ref) => {
  const wrapperRef = useRef(null);
  const gsapContextRef = useRef(null);

  useEffect(
    () => () => {
      gsapContextRef.current.revert();
    },
    [],
  );

  const playAnimation = () => {
    gsapContextRef.current = gsap.context(() => {
      gsap.to(wrapperRef.current, {
        y: 0,
        delay: 0.9,
        duration: transitions.SLOWEST,
        ease: CustomEase.create('custom', '.05,.4,.08,1'),
      });
    }, wrapperRef.current);
  };

  useImperativeHandle(
    ref,
    () => ({
      playAnimation,
    }),
    [],
  );

  const onClickWorks = () => {
    window.scrollTo(0, window.innerHeight * 1.4);
  };

  return (
    <div ref={wrapperRef} className={classNames(className, 'header')}>
      <Icon className="header__logo" name="logo" />

      <div className="header__navs">
        <Button onClick={onClickWorks} theme="transparent">
          Works
        </Button>

        <Button
          href="https://t.me/nikolaynrnr"
          theme="accent-second"
          icon="mail"
        />
      </div>
    </div>
  );
});

Header.displayName = 'Header';

Header.defaultProps = {
  className: '',
};

Header.propTypes = {
  className: PropTypes.string,
};

export default Header;
