import React from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';
import './noice.scss';

function Noice({ className = '' }) {
  return <div className={classNames(className, 'noice')} />;
}

Noice.propTypes = {
  className: PropTypes.string,
};

export default Noice;
