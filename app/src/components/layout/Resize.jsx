import React, { useEffect } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import useResize from '@/stores/useResize';
import useEventListener from '@/hooks/useEventListener';

import { calcFontSize, checkIsMobile, setVH } from '@/tools/resize';

function Resize({ children = () => {}, className = '' }) {
  const { setIsMobile, isMobile, setFontSize, fontSize } = useResize();

  const updateFontSize = () => {
    setFontSize(calcFontSize());
  };

  useEventListener(
    'resize',
    () => {
      const newIsMobile = checkIsMobile();

      if (newIsMobile !== isMobile) setIsMobile(newIsMobile);

      updateFontSize();

      setVH();
    },
    [isMobile],
    {
      immediate: true,
      layout: true,
    },
  );

  useEffect(setVH, []);

  return (
    <div
      style={{ fontSize: `${fontSize}px` }}
      className={classNames(className, {
        'is-mobile': isMobile,
        'is-desktop': !isMobile,
      })}
    >
      {children({ updateFontSize })}
    </div>
  );
}

Resize.propTypes = {
  className: PropTypes.string,
  children: PropTypes.func,
};

export default Resize;
