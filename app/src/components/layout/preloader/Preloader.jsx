import React, { useEffect, useRef, useState } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import gsap from '@/lib/gsap';
import CustomEase from '@/lib/gsap/CustomEase';

import './preloader.scss';

import resources from '@/config/resources';
import transitions from '@/config/transitions';

function Preloader({
  className = '',
  onComplete = () => {},
  onLoaded = () => {},
}) {
  const [countImagesLoaded, setCountImagesLoaded] = useState(0);
  const wrapperRef = useRef(null);

  useEffect(() => {
    const context = gsap.context(() => {
      Promise.all(
        resources.map((resource) =>
          fetch(resource).then(() =>
            setCountImagesLoaded(
              (currentCountImagesLoaded) => currentCountImagesLoaded + 1,
            ),
          ),
        ),
      ).then(() => {
        onLoaded();
        gsap.to(wrapperRef.current, {
          height: 0,
          duration: transitions.SLOWEST,
          delay: 0.7,
          ease: CustomEase.create('custom', '.05,.4,.08,1'),
          onComplete,
        });
      });
    });

    return () => {
      context.revert();
    };
  }, []);

  const width = `${(countImagesLoaded * 100) / resources.length}%`;

  return (
    <div ref={wrapperRef} className={classNames(className, 'preloader')}>
      <div className="preloader__wrapper">
        <div className="preloader__border">
          <div className="preloader__body">
            <div className="preloader__load-outline">
              <div className="preloader__load-line" style={{ width }} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Preloader.propTypes = {
  className: PropTypes.string,
  onComplete: PropTypes.func,
  onLoaded: PropTypes.func,
};

export default Preloader;
