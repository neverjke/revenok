import React, { Fragment, useEffect, useRef } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import gsap from '@/lib/gsap';

import './gallery.scss';

import galleryCards from '@/components/sections/gallery/galleryCards';

import Text from '@/components/ui/text/Text';
import useArrayRef from '@/hooks/useArrayRef';
import transitions from '@/config/transitions';

function Gallery({ className = '' }) {
  const wrapperRef = useRef(null);

  useEffect(() => {
    const context = gsap.context(() => {
      const timeline = gsap
        .timeline({
          scrollTrigger: {
            trigger: wrapperRef.current,
            start: 'top top',
            end: '+=300%',
            scrub: 1,
            pin: true,
            pinSpacing: false,
          },
        })
        .duration(1.6);

      timeline.addLabel('start');

      timeline.to('.gallery__cards', {
        rotateY: '-40deg',
        duration: 1.6,
        onUpdate() {
          const rotateY = gsap.getProperty('.gallery__cards', 'rotateY');

          gsap.set('.gallery__card-content', {
            rotateY: -rotateY,
          });
        },
      });

      timeline.to(
        '.gallery__line, .gallery__headline',
        {
          y: window.innerHeight / 2,
          opacity: 0,
          duration: 0.25,
        },
        'start+=0',
      );

      timeline.to(
        '.gallery__blur',
        {
          opacity: 1,
          duration: transitions.MEDIUM,
        },
        'start+=0',
      );

      timeline.to(
        '.gallery__card-content',
        {
          opacity: 1,
          duration: 0.1,
          stagger: {
            each: 0.2,
          },
        },
        'start+=0.15',
      );

      timeline.to(
        '.gallery__card-content',
        {
          opacity: 0,
          duration: 0.1,
          stagger: {
            each: 0.2,
          },
        },
        'start+=0.35',
      );
    }, wrapperRef.current);

    return () => {
      context.revert();
    };
  }, []);

  return (
    <div ref={wrapperRef} className={classNames(className, 'gallery')}>
      <div className="gallery__line" />
      <Text className="gallery__headline" text="MY WORKS" />

      <div className="gallery__scene">
        <div className="gallery__cards gallery__cards--gallery">
          <img
            className="gallery__blur"
            src="/images/blurs/blur-gallery.png"
            alt="background-blur"
          />

          {galleryCards.map((card, index) => (
            <a
              key={index}
              href={card.webUrl}
              target="_blank"
              className="gallery__card"
              style={{
                '--gallery-index': index,
                backgroundImage: `linear-gradient(180deg, rgba(4, 0, 27, 0.25) 0%, rgba(1, 0, 8, 0.30) 68.9%, rgba(0, 0, 0, 0.60) 100%), url(${card.imageUrl})`,
                backgroundSize: 'contain',
                backgroundRepeat: 'no-repeat',
              }}
              rel="noreferrer"
            />
          ))}
        </div>
        <div className="gallery__cards gallery__cards--content">
          {galleryCards.map((card, index) => (
            <div
              key={index}
              className="gallery__card-content"
              style={{ '--gallery-index': index }}
            >
              <p
                className="gallery__description"
                dangerouslySetInnerHTML={{ __html: card.description }}
              />

              <Text className="gallery__title" tag="p" text={card.title} />

              {card.techs.map((tech, techIndex) => (
                <Text
                  key={techIndex}
                  className={classNames(
                    className,
                    'gallery__techs',
                    `gallery__techs--tech-${techIndex}`,
                  )}
                  text={tech}
                />
              ))}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

Gallery.propTypes = {
  className: PropTypes.string,
};

export default Gallery;
