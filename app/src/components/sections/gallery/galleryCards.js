const galleryCards = [
  {
    title: 'Tobto',
    description: 'Photo share service</br> for photographers',
    techs: ['REACT', 'VITE', 'SCSS', 'ZUSTAND'],
    imageUrl: '/images/sample-tobto.jpg',
    webUrl: 'https://dev.tobto.io/uk/sign-in',
  },
  {
    title: 'Nft Dolphins',
    description: 'Landing page for nft platform with animations',
    techs: ['REACT', 'VITE', 'SCSS', 'GSAP'],
    imageUrl: '/images/sample-dolphins.jpg',
    webUrl: 'https://www.dauntlessdolphins.com/',
  },
  {
    title: 'Rzm',
    description: 'Multi page website</br> with test functionality',
    techs: ['VUE', 'VITE', 'SCSS'],
    imageUrl: '/images/sample-pzm.jpg',
    webUrl: 'https://rzm.pmprod.ru/',
  },
];

export default galleryCards;
