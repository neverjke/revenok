import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import gsap from '@/lib/gsap';
import CustomEase from '@/lib/gsap/CustomEase';

import './origin.scss';

import transitions from '@/config/transitions';

import Text from '@/components/ui/text/Text';
import Button from '@/components/ui/button/Button';

const Origin = forwardRef(({ className }, ref) => {
  const wrapperRef = useRef(null);
  const gsapContextRef = useRef(null);

  useEffect(
    () => () => {
      gsapContextRef.current.revert();
    },
    [],
  );

  const playAnimation = () => {
    gsapContextRef.current = gsap.context(() => {
      const timeline = gsap.timeline();

      timeline.to('.origin__from-bottom', {
        y: 0,
        duration: transitions.SLOWER,
        delay: 0.8,
        ease: CustomEase.create('custom', '.05,.4,.08,1'),
        stagger: {
          each: 0.05,
        },
      });

      timeline.to(
        '.origin__blur',
        {
          y: 0,
          opacity: 1,
          duration: transitions.SLOW,
          ease: CustomEase.create('custom', '.05,.4,.08,1'),
        },
        '<',
      );

      timeline.to(
        '.origin__mouse-scroll',
        {
          y: 0,
          opacity: 1,
          duration: transitions.SLOW,
          ease: CustomEase.create('custom', '.05,.4,.08,1'),
        },
        '<0.1',
      );
    }, wrapperRef.current);
  };

  useImperativeHandle(
    ref,
    () => ({
      playAnimation,
    }),
    [],
  );

  const onClickSkill = () => {
    window.scrollTo(0, window.innerHeight * 0.75);
  };

  return (
    <div ref={wrapperRef} className={classNames(className, 'origin')}>
      <img
        className="origin__blur"
        src="/images/blurs/blur-origin.png"
        alt="background-blur"
        data-speed="0.5"
      />

      <div className="origin__scene">
        <div className="origin__scene-content">
          <div data-speed="1.05" className="origin__image-wrapper">
            <img
              className="origin__image"
              src="/images/origin.jpg"
              alt="image"
            />
          </div>

          <div
            data-speed="0.95"
            className="origin__text-wrapper origin__text-wrapper--name"
          >
            <Text
              className="origin__text origin__from-bottom"
              text="NICKOLAY"
            />
          </div>

          <div
            data-speed="0.90"
            className="origin__text-wrapper origin__text-wrapper--surname"
          >
            <Text className="origin__text origin__from-bottom" text="REVENOK" />
          </div>

          <div
            data-speed="0.85"
            className="origin__text-wrapper origin__text-wrapper--spec"
          >
            <Text
              className="origin__text origin__from-bottom"
              text="FRONTEND"
            />
          </div>

          <div data-speed="0.90" className="origin__button">
            <div className="origin__from-bottom">
              <Button onClick={onClickSkill} theme="accent">
                Tech skills
              </Button>
            </div>
          </div>
        </div>
      </div>

      <div data-speed="0.95" className="origin__mouse-scroll-wrapper">
        <div className="origin__mouse-scroll">
          <div className="origin__icon origin__icon--mouse" />
          <div className="origin__icon origin__icon--scroll" />
        </div>
      </div>
    </div>
  );
});

Origin.displayName = 'Origin';

Origin.defaultProps = {
  className: '',
};

Origin.propTypes = {
  className: PropTypes.string,
};

export default Origin;
