import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import './skill.scss';

import gsap from '@/lib/gsap';
import CustomEase from '@/lib/gsap/CustomEase';
import Text from '@/components/ui/text/Text';
import transitions from '@/config/transitions';

const Skill = forwardRef(({ className = '' }, ref) => {
  const wrapperRef = useRef(null);
  const gsapContextRef = useRef(null);

  useEffect(
    () => () => {
      gsapContextRef.current.revert();
    },
    [],
  );

  const playAnimation = () => {
    gsapContextRef.current = gsap.context(() => {
      gsap.to(wrapperRef.current, {
        y: 0,
        delay: 0.7,
        duration: transitions.SLOWEST,
        ease: CustomEase.create('custom', '.05,.4,.08,1'),
      });
    }, wrapperRef.current);
  };

  useImperativeHandle(
    ref,
    () => ({
      playAnimation,
    }),
    [],
  );

  return (
    <div ref={wrapperRef} className={classNames(className, 'skill')}>
      <Text className="skill__headline" text="TECH SKILLS" />

      <div className="skill__review">
        <p className="skill__text">
          <Text text="I make websites with a focus on" />{' '}
          <Text
            className="skill__accent skill__accent--accent-second"
            text="quality"
          />
          {'. '}
          <Text text="Achieving this through the use of" />{' '}
          <Text
            className="skill__accent skill__accent--accent-third"
            text="modern technology"
          />
          {'. '}
        </p>

        <div className="skill__tools">
          <Text className="skill__accent" text="REACT" /> {' • '}
          <Text
            className="skill__accent skill__accent--accent-third"
            text="GSAP"
          />{' '}
          {' • '}
          <Text
            className="skill__accent skill__accent--accent-second"
            text="SCSS"
          />{' '}
          {' • '}
          <Text className="skill__accent" text="VITE" />
        </div>
      </div>
    </div>
  );
});

Skill.propTypes = {
  className: PropTypes.string,
};

export default Skill;
