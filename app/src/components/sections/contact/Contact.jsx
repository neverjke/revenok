import React, { useEffect, useRef } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import './contact.scss';

import gsap from '@/lib/gsap';
import { ScrollTrigger } from '@/lib/gsap/ScrollTrigger';

import Text from '@/components/ui/text/Text';
import Button from '@/components/ui/button/Button';

gsap.registerPlugin(ScrollTrigger);

function Contact({ className = '' }) {
  const wrapperRef = useRef(null);

  useEffect(() => {
    const context = gsap.context(() => {
      gsap.to('.contact__from-bottom', {
        scrollTrigger: {
          trigger: wrapperRef.current,
          start: 'top top+=50%',
          end: '+=50%',
          scrub: 1,
          anticipatePin: 1,
        },
        y: 0,
        stagger: {
          each: 0.05,
        },
      });
    }, wrapperRef.current);

    return () => {
      context.revert();
    };
  }, []);

  return (
    <div ref={wrapperRef} className={classNames(className, 'contact')}>
      <img
        className="contact__blur"
        src="/images/blurs/blur-contact.png"
        alt="background-blur"
        data-speed="0.5"
      />

      <div className="contact__text-line contact__text-line--first">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="ALSO LOOKING"
        />
      </div>
      <div className="contact__text-line contact__text-line--second">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="FOR"
        />
      </div>
      <div className="contact__text-line contact__text-line--third">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="A GOOD JOB"
        />
      </div>

      {/* MOBILE */}
      <div className="contact__text-line contact__text-line--mobile contact__text-line--first">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="ALSO"
        />
      </div>
      <div className="contact__text-line contact__text-line--mobile contact__text-line--second">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="LOOKING"
        />
      </div>
      <div className="contact__text-line contact__text-line--mobile contact__text-line--third">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="FOR A GOOD"
        />
      </div>
      <div className="contact__text-line contact__text-line--mobile contact__text-line--fourth">
        <Text
          className="contact__text contact__from-bottom"
          tag="p"
          text="JOB"
        />
      </div>

      <div className="contact__button">
        <div className="contact__from-bottom">
          <Button href="https://t.me/nikolaynrnr" theme="accent">
            Telegram
          </Button>
        </div>
      </div>
    </div>
  );
}

Contact.propTypes = {
  className: PropTypes.string,
};

export default Contact;
