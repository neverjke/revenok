/*!
 * CustomWiggle 3.11.1
 * https://greensock.com
 *
 * @license Copyright 2022, GreenSock. All rights reserved.
 * *** DO NOT DEPLOY THIS FILE ***
 * This is a trial version that only works locally and on domains like codepen.io and codesandbox.io.
 * Loading it on an unauthorized domain violates the license and will cause a redirect.
 * Get the unrestricted file by joining Club GreenSock at https://greensock.com/club
 * @author: Jack Doyle, jack@greensock.com
 */

let e,
  t,
  n,
  o = () =>
    e ||
    ('undefined' != typeof window &&
      (e = window.gsap) &&
      e.registerPlugin &&
      e),
  i = {
    easeOut: 'M0,1,C0.7,1,0.6,0,1,0',
    easeInOut: 'M0,0,C0.1,0,0.24,1,0.444,1,0.644,1,0.6,0,1,0',
    anticipate:
      'M0,0,C0,0.222,0.024,0.386,0,0.4,0.18,0.455,0.65,0.646,0.7,0.67,0.9,0.76,1,0.846,1,1',
    uniform: 'M0,0,C0,0.95,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1,0',
  },
  a = (e) => e,
  s = (a) => {
    if (!t)
      if (((e = o()), (n = e && e.parseEase('_CE')), n)) {
        for (let e in i) i[e] = n('', i[e]);
        (t = 1),
          (d('wiggle').config = (e) =>
            'object' == typeof e
              ? d('', e)
              : d('wiggle(' + e + ')', { wiggles: +e }));
      } else
        a &&
          console.warn('Please gsap.registerPlugin(CustomEase, CustomWiggle)');
  },
  r = (t, o) => (
    'function' != typeof t && (t = e.parseEase(t) || n('', t)),
    t.custom || !o ? t : (e) => 1 - t(e)
  ),
  g = function () {
    return String.fromCharCode.apply(null, arguments);
  },
  u = g(103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109),
  l = true,
  d = (e, o) => {
    t || s(1);
    let g,
      u,
      d,
      c,
      f,
      p,
      w,
      h,
      m,
      y = 0 | ((o = o || {}).wiggles || 10),
      C = 1 / y,
      E = C / 2,
      M = 'anticipate' === o.type,
      O = i[o.type] || i.easeOut,
      v = a;
    if (l) {
      if (
        (M && ((v = O), (O = i.easeOut)),
        o.timingEase && (v = r(o.timingEase)),
        o.amplitudeEase && (O = r(o.amplitudeEase, !0)),
        (p = v(E)),
        (w = M ? -O(E) : O(E)),
        (h = [0, 0, p / 4, 0, p / 2, w, p, w]),
        'random' === o.type)
      ) {
        for (
          h.length = 4, g = v(C), u = 2 * Math.random() - 1, m = 2;
          m < y;
          m++
        )
          (E = g),
            (w = u),
            (g = v(C * m)),
            (u = 2 * Math.random() - 1),
            (d = Math.atan2(u - h[h.length - 3], g - h[h.length - 4])),
            (c = Math.cos(d) * C),
            (f = Math.sin(d) * C),
            h.push(E - c, w - f, E, w, E + c, w + f);
        h.push(g, 0, 1, 0);
      } else {
        for (m = 1; m < y; m++)
          h.push(v(E + C / 2), w),
            (E += C),
            (w = (w > 0 ? -1 : 1) * O(m * C)),
            (p = v(E)),
            h.push(v(E - C / 2), w, p, w);
        h.push(v(E + C / 4), w, v(E + C / 4), 0, 1, 0);
      }
      for (m = h.length; --m > -1; ) h[m] = ~~(1e3 * h[m]) / 1e3;
      return (h[2] = 'C' + h[2]), n(e, 'M' + h.join(','));
    }
  };
class c {
  constructor(e, t) {
    this.ease = d(e, t);
  }
  static create(e, t) {
    return d(e, t);
  }
  static register(t) {
    (e = t), s();
  }
}
o() && e.registerPlugin(c), (c.version = '3.11.1');
export default c;
export { c as CustomWiggle };
