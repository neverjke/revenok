/*!
 * CustomEase 3.11.1
 * https://greensock.com
 *
 * @license Copyright 2022, GreenSock. All rights reserved.
 * Subject to the terms at https://greensock.com/standard-license or for Club GreenSock members, the agreement issued with that membership.
 * @author: Jack Doyle, jack@greensock.com
 */

const e = /[achlmqstvz]|(-?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi;
const t = /[\+\-]?\d*\.?\d+e[\+\-]?\d+/gi;
const n = Math.PI / 180;
const s = Math.sin;
const i = Math.cos;
const o = Math.abs;
const a = Math.sqrt;
const r = (e) => Math.round(1e5 * e) / 1e5 || 0;
function l(e, t, r, l, h, c, g, u, f) {
  if (e === u && t === f) return;
  (r = o(r)), (l = o(l));
  const d = (h % 360) * n;
  const x = i(d);
  const p = s(d);
  const y = Math.PI;
  const w = 2 * y;
  const m = (e - u) / 2;
  const M = (t - f) / 2;
  let C = x * m + p * M;
  let E = -p * m + x * M;
  const b = C * C;
  const v = E * E;
  const S = b / (r * r) + v / (l * l);
  S > 1 && ((r = a(S) * r), (l = a(S) * l));
  const N = r * r;
  const L = l * l;
  let O = (N * L - N * v - L * b) / (N * v + L * b);
  O < 0 && (O = 0);
  const P = (c === g ? -1 : 1) * a(O);
  const A = P * ((r * E) / l);
  const D = P * ((-l * C) / r);
  const V = (e + u) / 2 + (x * A - p * D);
  const T = (t + f) / 2 + (p * A + x * D);
  let _ = (C - A) / r;
  let q = (E - D) / l;
  const G = (-C - A) / r;
  const I = (-E - D) / l;
  const R = _ * _ + q * q;
  let W = (q < 0 ? -1 : 1) * Math.acos(_ / a(R));
  let j =
    (_ * I - q * G < 0 ? -1 : 1) *
    Math.acos((_ * G + q * I) / a(R * (G * G + I * I)));
  isNaN(j) && (j = y),
    !g && j > 0 ? (j -= w) : g && j < 0 && (j += w),
    (W %= w),
    (j %= w);
  let z;
  const H = Math.ceil(o(j) / (w / 4));
  const Q = [];
  const Z = j / H;
  const U = ((4 / 3) * s(Z / 2)) / (1 + i(Z / 2));
  const Y = x * r;
  const k = p * r;
  const B = p * -l;
  const F = x * l;
  for (z = 0; z < H; z++) {
    (C = i((h = W + z * Z))),
      (E = s(h)),
      (_ = i((h += Z))),
      (q = s(h)),
      Q.push(C - U * E, E + U * C, _ + U * q, q - U * _, _, q);
  }
  for (z = 0; z < Q.length; z += 2) {
    (C = Q[z]),
      (E = Q[z + 1]),
      (Q[z] = C * Y + E * B + V),
      (Q[z + 1] = C * k + E * F + T);
  }
  return (Q[z - 2] = u), (Q[z - 1] = f), Q;
}
let h;
let c;
const g = () =>
  h ||
  (typeof window !== 'undefined' && (h = window.gsap) && h.registerPlugin && h);
const u = () => {
  (h = g()),
    h
      ? (h.registerEase('_CE', M.create), (c = 1))
      : console.warn('Please gsap.registerPlugin(CustomEase)');
};
const f = (e) => ~~(1e3 * e + (e < 0 ? -0.5 : 0.5)) / 1e3;
const d = function () {
  return String.fromCharCode.apply(null, arguments);
};
const x = d(103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109);
const y = /[-+=\.]*\d+[\.e\-\+]*\d*[e\-\+]*\d*/gi;
const w = /[cLlsSaAhHvVtTqQ]/g;
const m = function (e, t, n, s, i, o, a, r, l, h, c) {
  let g;
  const u = (e + n) / 2;
  const f = (t + s) / 2;
  const d = (n + i) / 2;
  const x = (s + o) / 2;
  const p = (i + a) / 2;
  const y = (o + r) / 2;
  const w = (u + d) / 2;
  const M = (f + x) / 2;
  const C = (d + p) / 2;
  const E = (x + y) / 2;
  const b = (w + C) / 2;
  const v = (M + E) / 2;
  const S = a - e;
  const N = r - t;
  const L = Math.abs((n - a) * N - (s - r) * S);
  const O = Math.abs((i - a) * N - (o - r) * S);
  return (
    h ||
      ((h = [
        { x: e, y: t },
        { x: a, y: r },
      ]),
      (c = 1)),
    h.splice(c || h.length - 1, 0, { x: b, y: v }),
    (L + O) * (L + O) > l * (S * S + N * N) &&
      ((g = h.length),
      m(e, t, u, f, w, M, b, v, l, h, c),
      m(b, v, C, E, p, y, a, r, l, h, c + 1 + (h.length - g))),
    h
  );
};
class M {
  constructor(e, t, n) {
    c || u(), (this.id = e), this.setData(t, n);
  }

  setData(n, s) {
    s = s || {};
    let i;
    let a;
    let r;
    let c;
    let g;
    let u;
    let f;
    let d;
    let x;
    let p = (n = n || '0,0,1,1').match(y);
    let M = 1;
    const C = [];
    const E = [];
    const b = s.precision || 1;
    let v = b <= 1;
    if (
      ((this.data = n),
      (w.test(n) || (~n.indexOf('M') && n.indexOf('C') < 0)) &&
        (p = (function (n) {
          let s;
          let i;
          let a;
          let r;
          let h;
          let c;
          let g;
          let u;
          let f;
          let d;
          let x;
          let p;
          let y;
          let w;
          let m;
          const M =
            `${n}`
              .replace(t, (e) => {
                const t = +e;
                return t < 1e-4 && t > -1e-4 ? 0 : t;
              })
              .match(e) || [];
          const C = [];
          let E = 0;
          let b = 0;
          const v = M.length;
          let S = 0;
          const N = `ERROR: malformed path: ${n}`;
          const L = function (e, t, n, s) {
            (d = (n - e) / 3),
              (x = (s - t) / 3),
              g.push(e + d, t + x, n - d, s - x, n, s);
          };
          if (!n || !isNaN(M[0]) || isNaN(M[1])) return console.log(N), C;
          for (s = 0; s < v; s++) {
            if (
              ((y = h),
              isNaN(M[s]) ? ((h = M[s].toUpperCase()), (c = h !== M[s])) : s--,
              (a = +M[s + 1]),
              (r = +M[s + 2]),
              c && ((a += E), (r += b)),
              s || ((u = a), (f = r)),
              h === 'M')
            ) {
              g && (g.length < 8 ? (C.length -= 1) : (S += g.length)),
                (E = u = a),
                (b = f = r),
                (g = [a, r]),
                C.push(g),
                (s += 2),
                (h = 'L');
            } else if (h === 'C') {
              g || (g = [0, 0]),
                c || (E = b = 0),
                g.push(
                  a,
                  r,
                  E + 1 * M[s + 3],
                  b + 1 * M[s + 4],
                  (E += 1 * M[s + 5]),
                  (b += 1 * M[s + 6]),
                ),
                (s += 6);
            } else if (h === 'S') {
              (d = E),
                (x = b),
                (y !== 'C' && y !== 'S') ||
                  ((d += E - g[g.length - 4]), (x += b - g[g.length - 3])),
                c || (E = b = 0),
                g.push(d, x, a, r, (E += 1 * M[s + 3]), (b += 1 * M[s + 4])),
                (s += 4);
            } else if (h === 'Q') {
              (d = E + (2 / 3) * (a - E)),
                (x = b + (2 / 3) * (r - b)),
                c || (E = b = 0),
                (E += 1 * M[s + 3]),
                (b += 1 * M[s + 4]),
                g.push(
                  d,
                  x,
                  E + (2 / 3) * (a - E),
                  b + (2 / 3) * (r - b),
                  E,
                  b,
                ),
                (s += 4);
            } else if (h === 'T') {
              (d = E - g[g.length - 4]),
                (x = b - g[g.length - 3]),
                g.push(
                  E + d,
                  b + x,
                  a + (2 / 3) * (E + 1.5 * d - a),
                  r + (2 / 3) * (b + 1.5 * x - r),
                  (E = a),
                  (b = r),
                ),
                (s += 2);
            } else if (h === 'H') L(E, b, (E = a), b), (s += 1);
            else if (h === 'V') L(E, b, E, (b = a + (c ? b - E : 0))), (s += 1);
            else if (h === 'L' || h === 'Z') {
              h === 'Z' && ((a = u), (r = f), (g.closed = !0)),
                (h === 'L' || o(E - a) > 0.5 || o(b - r) > 0.5) &&
                  (L(E, b, a, r), h === 'L' && (s += 2)),
                (E = a),
                (b = r);
            } else if (h === 'A') {
              if (
                ((w = M[s + 4]),
                (m = M[s + 5]),
                (d = M[s + 6]),
                (x = M[s + 7]),
                (i = 7),
                w.length > 1 &&
                  (w.length < 3
                    ? ((x = d), (d = m), i--)
                    : ((x = m), (d = w.substr(2)), (i -= 2)),
                  (m = w.charAt(1)),
                  (w = w.charAt(0))),
                (p = l(
                  E,
                  b,
                  +M[s + 1],
                  +M[s + 2],
                  +M[s + 3],
                  +w,
                  +m,
                  (c ? E : 0) + 1 * d,
                  (c ? b : 0) + 1 * x,
                )),
                (s += i),
                p)
              )
                for (i = 0; i < p.length; i++) g.push(p[i]);
              (E = g[g.length - 2]), (b = g[g.length - 1]);
            } else console.log(N);
          }
          return (
            (s = g.length),
            s < 6
              ? (C.pop(), (s = 0))
              : g[0] === g[s - 2] && g[1] === g[s - 1] && (g.closed = !0),
            (C.totalPoints = S + s),
            C
          );
        })(n)[0]),
      (i = p.length),
      i === 4)
    )
      p.unshift(0, 0), p.push(1, 1), (i = 8);
    else if ((i - 2) % 6) throw 'Invalid CustomEase';
    for (
      (+p[0] == 0 && +p[i - 2] == 1) ||
        ((e, t, n) => {
          n || n === 0 || (n = Math.max(+e[e.length - 1], +e[1]));
          let s;
          const i = -1 * +e[0];
          const o = -n;
          const a = e.length;
          const r = 1 / (+e[a - 2] + i);
          let l =
            -t ||
            (Math.abs(+e[a - 1] - +e[1]) < 0.01 * (+e[a - 2] - +e[0])
              ? ((e) => {
                  let t;
                  const n = e.length;
                  let s = 1e20;
                  for (t = 1; t < n; t += 6) +e[t] < s && (s = +e[t]);
                  return s;
                })(e) + o
              : +e[a - 1] + o);
          for (l = l ? 1 / l : -r, s = 0; s < a; s += 2)
            (e[s] = (+e[s] + i) * r), (e[s + 1] = (+e[s + 1] + o) * l);
        })(p, s.height, s.originY),
        this.segment = p,
        c = 2;
      c < i;
      c += 6
    ) {
      (a = { x: +p[c - 2], y: +p[c - 1] }),
        (r = { x: +p[c + 4], y: +p[c + 5] }),
        C.push(a, r),
        m(
          a.x,
          a.y,
          +p[c],
          +p[c + 1],
          +p[c + 2],
          +p[c + 3],
          r.x,
          r.y,
          1 / (2e5 * b),
          C,
          C.length - 1,
        );
    }
    for (i = C.length, c = 0; c < i; c++) {
      (f = C[c]),
        (d = C[c - 1] || f),
        (f.x > d.x || (d.y !== f.y && d.x === f.x) || f === d) && f.x <= 1
          ? ((d.cx = f.x - d.x),
            (d.cy = f.y - d.y),
            (d.n = f),
            (d.nx = f.x),
            v &&
              c > 1 &&
              Math.abs(d.cy / d.cx - C[c - 2].cy / C[c - 2].cx) > 2 &&
              (v = 0),
            d.cx < M &&
              (d.cx
                ? (M = d.cx)
                : ((d.cx = 0.001),
                  c === i - 1 &&
                    ((d.x -= 0.001), (M = Math.min(M, 0.001)), (v = 0)))))
          : (C.splice(c--, 1), i--);
    }
    if (((i = (1 / M + 1) | 0), (g = 1 / i), (u = 0), (f = C[0]), v)) {
      for (c = 0; c < i; c++) {
        (x = c * g),
          f.nx < x && (f = C[++u]),
          (a = f.y + ((x - f.x) / f.cx) * f.cy),
          (E[c] = { x, cx: g, y: a, cy: 0, nx: 9 }),
          c && (E[c - 1].cy = a - E[c - 1].y);
      }
      E[i - 1].cy = C[C.length - 1].y - a;
    } else {
      for (c = 0; c < i; c++) f.nx < c * g && (f = C[++u]), (E[c] = f);
      u < C.length - 1 && (E[c - 1] = C[C.length - 2]);
    }
    return (
      (this.ease = (e) => {
        let t = E[(e * i) | 0] || E[i - 1];
        return t.nx < e && (t = t.n), t.y + ((e - t.x) / t.cx) * t.cy;
      }),
      (this.ease.custom = this),
      this.id && h && h.registerEase(this.id, this.ease),
      this
    );
  }

  getSVGData(e) {
    return M.getSVGData(this, e);
  }

  static create(e, t, n) {
    return new M(e, t, n).ease;
  }

  static register(e) {
    (h = e), u();
  }

  static get(e) {
    return h.parseEase(e);
  }

  static getSVGData(e, t) {
    let n;
    let s;
    let i;
    let o;
    let a;
    let l;
    let c;
    let g;
    let u;
    let d;
    const x = (t = t || {}).width || 100;
    let p = t.height || 100;
    const y = t.x || 0;
    let w = (t.y || 0) + p;
    const m = h.utils.toArray(t.path)[0];
    if (
      (t.invert && ((p = -p), (w = 0)),
      typeof e === 'string' && (e = h.parseEase(e)),
      e.custom && (e = e.custom),
      e instanceof M)
    ) {
      n = (function (e) {
        typeof e[0] === 'number' && (e = [e]);
        let t;
        let n;
        let s;
        let i;
        let o = '';
        const a = e.length;
        for (n = 0; n < a; n++) {
          for (
            i = e[n], o += `M${r(i[0])},${r(i[1])} C`, t = i.length, s = 2;
            s < t;
            s++
          ) {
            o += `${r(i[s++])},${r(i[s++])} ${r(i[s++])},${r(i[s++])} ${r(
              i[s++],
            )},${r(i[s])} `;
          }
          i.closed && (o += 'z');
        }
        return o;
      })(
        (function (e, t, n, s, i, o, a) {
          let r;
          let l;
          let h;
          let c;
          let g;
          let u = e.length;
          for (; --u > -1; ) {
            for (r = e[u], l = r.length, h = 0; h < l; h += 2) {
              (c = r[h]),
                (g = r[h + 1]),
                (r[h] = c * t + g * s + o),
                (r[h + 1] = c * n + g * i + a);
            }
          }
          return (e._dirty = 1), e;
        })([e.segment], x, 0, 0, -p, y, w),
      );
    } else {
      for (
        n = [y, w],
          c = Math.max(5, 200 * (t.precision || 1)),
          o = 1 / c,
          c += 2,
          g = 5 / c,
          u = f(y + o * x),
          d = f(w + e(o) * -p),
          s = (d - w) / (u - y),
          i = 2;
        i < c;
        i++
      ) {
        (a = f(y + i * o * x)),
          (l = f(w + e(i * o) * -p)),
          (Math.abs((l - d) / (a - u) - s) > g || i === c - 1) &&
            (n.push(u, d), (s = (l - d) / (a - u))),
          (u = a),
          (d = l);
      }
      n = `M${n.join(',')}`;
    }
    return m && m.setAttribute('d', n), n;
  }
}
g() && h.registerPlugin(M), (M.version = '3.11.1');
export default M;
export { M as CustomEase };
