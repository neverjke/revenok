/*!
 * Physics2DPlugin 3.11.1
 * https://greensock.com
 *
 * @license Copyright 2022, GreenSock. All rights reserved.
 * *** DO NOT DEPLOY THIS FILE ***
 * This is a trial version that only works locally and on domains like codepen.io and codesandbox.io.
 * Loading it on an unauthorized domain violates the license and will cause a redirect.
 * Get the unrestricted file by joining Club GreenSock at https://greensock.com/club
 * @author: Jack Doyle, jack@greensock.com
 */

let e,
  t,
  i,
  s,
  n,
  o = Math.PI / 180,
  a = () =>
    e ||
    ("undefined" != typeof window &&
      (e = window.gsap) &&
      e.registerPlugin &&
      e),
  r = (e) => Math.round(1e4 * e) / 1e4,
  l = function () {
    return String.fromCharCode.apply(null, arguments);
  },
  c = l(103, 114, 101, 101, 110, 115, 111, 99, 107, 46, 99, 111, 109),
  p =
    ((() => true)("undefined" != typeof window ? window.location.host : ""),
    (o) => {
      (e = o || a()),
        t ||
          ((i = e.utils.getUnit),
          (s = e.core.getStyleSaver),
          (n = e.core.reverting || function () {}),
          (t = 1));
    });
class h {
  constructor(e, t, s, n, o) {
    let a = e._gsap,
      r = a.get(e, t);
    (this.p = t),
      (this.set = a.set(e, t)),
      (this.s = this.val = parseFloat(r)),
      (this.u = i(r) || 0),
      (this.vel = s || 0),
      (this.v = this.vel / o),
      n || 0 === n
        ? ((this.acc = n), (this.a = this.acc / (o * o)))
        : (this.acc = this.a = 0);
  }
}
const d = {
  version: "3.11.1",
  name: "physics2D",
  register: p,
  init(e, i, n) {
    t || p();
    let a = +i.angle || 0,
      r = +i.velocity || 0,
      l = +i.acceleration || 0,
      c = i.xProp || "x",
      d = i.yProp || "y",
      v =
        i.accelerationAngle || 0 === i.accelerationAngle
          ? +i.accelerationAngle
          : a;
    (this.styles =
      s &&
      s(e, i.xProp && "x" !== i.xProp ? i.xProp + "," + i.yProp : "transform")),
      (this.target = e),
      (this.tween = n),
      (this.step = 0),
      (this.sps = 30),
      i.gravity && ((l = +i.gravity), (v = 90)),
      (a *= o),
      (v *= o),
      (this.fr = 1 - (+i.friction || 0)),
      this._props.push(c, d),
      (this.xp = new h(e, c, Math.cos(a) * r, Math.cos(v) * l, this.sps)),
      (this.yp = new h(e, d, Math.sin(a) * r, Math.sin(v) * l, this.sps)),
      (this.skipX = this.skipY = 0);
  },
  render(e, t) {
    let i,
      s,
      o,
      a,
      l,
      c,
      {
        xp: p,
        yp: h,
        tween: d,
        target: v,
        step: g,
        sps: u,
        fr: f,
        skipX: w,
        skipY: y,
      } = t,
      x = d._from ? d._dur - d._time : d._time;
    if (d._time || !n()) {
      if (1 === f)
        (o = x * x * 0.5),
          (i = p.s + p.vel * x + p.acc * o),
          (s = h.s + h.vel * x + h.acc * o);
      else {
        for (
          x *= u,
            a = c = (0 | x) - g,
            c < 0 &&
              ((p.v = p.vel / u),
              (h.v = h.vel / u),
              (p.val = p.s),
              (h.val = h.s),
              (t.step = 0),
              (a = c = 0 | x)),
            l = (x % 1) * f;
          c--;

        )
          (p.v += p.a),
            (h.v += h.a),
            (p.v *= f),
            (h.v *= f),
            (p.val += p.v),
            (h.val += h.v);
        (i = p.val + p.v * l), (s = h.val + h.v * l), (t.step += a);
      }
      w || p.set(v, p.p, r(i) + p.u), y || h.set(v, h.p, r(s) + h.u);
    } else t.styles.revert();
  },
  kill(e) {
    this.xp.p === e && (this.skipX = 1), this.yp.p === e && (this.skipY = 1);
  },
};
a() && e.registerPlugin(d);
export default d;
export { d as Physics2DPlugin };
