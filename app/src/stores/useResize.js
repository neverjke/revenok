import { create } from 'zustand';
import { calcFontSize, checkIsMobile } from '@/tools/resize';

const useResize = create((set) => ({
  isMobile: checkIsMobile(),
  fontSize: calcFontSize(),
  setIsMobile: (isMobile) => set({ isMobile }),
  setFontSize: (fontSize) => set({ fontSize }),
}));

export default useResize;
