const resize = {
  defaultFont: 16,

  desktop: {
    defaultWidth: 1440,
    minWidth: 540,
    defaultHeight: 800,
    minHeight: 0,
  },

  mobile: {
    defaultWidth: 360,
    minWidth: 280,
    defaultHeight: 0,
    minHeight: 0,
  },
};

export default resize;
