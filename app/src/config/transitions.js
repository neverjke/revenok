const transitions = {
  MEDIUM: 0.3,
  SLOW: 0.4,
  SLOWER: 0.5,
  SLOWEST: 0.6,
};

export default transitions;
