const resources = [
  '/images/origin.jpg',
  '/images/sample-tobto.jpg',
  '/images/sample-dolphins.jpg',
  '/images/sample-pzm.jpg',
  '/images/blurs/blur-origin.png',
  '/images/blurs/blur-gallery.png',
  '/images/blurs/blur-contact.png',
];

export default resources;
