import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import './main.scss';

import Header from '@/components/layout/header/Header';
import Origin from '@/components/sections/origin/Origin';
import Skill from '@/components/sections/skill/Skill';
import Gallery from '@/components/sections/gallery/Gallery';
import Contact from '@/components/sections/contact/Contact';

const Main = forwardRef(({ className }, ref) => {
  const headerRef = useRef(null);
  const originRef = useRef(null);
  const skillsRef = useRef(null);

  const playAnimation = () => {
    headerRef.current.playAnimation();
    originRef.current.playAnimation();
    skillsRef.current.playAnimation();
  };

  useImperativeHandle(
    ref,
    () => ({
      playAnimation,
    }),
    [],
  );

  return (
    <div className={classNames(className, 'main')}>
      <Header ref={headerRef} className="main__header" />
      <Origin ref={originRef} className="main__origin" />
      <Skill ref={skillsRef} className="main__skill" />
      <Gallery className="main__gallery" />
      <Contact className="main__contact" />
    </div>
  );
});

Main.displayName = 'Main';

Main.defaultProps = {
  className: '',
};

Main.propTypes = {
  className: PropTypes.string,
};

export default Main;
