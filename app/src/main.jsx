import React from 'react';
import ReactDOM from 'react-dom/client';
import App from '@/App';
import '@/styles/app.scss';

import gsap from '@/lib/gsap';
import ScrollTrigger from '@/lib/gsap/ScrollTrigger';
import CustomEase from '@/lib/gsap/CustomEase';
import ScrollSmoother from '@/lib/gsap/ScrollSmoother';
import SplitText from '@/lib/gsap/SplitText';

gsap.registerPlugin(ScrollSmoother, ScrollTrigger, CustomEase, SplitText);

ReactDOM.createRoot(document.querySelector('#app')).render(<App />);
