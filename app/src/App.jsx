import React, { useEffect, useRef, useState } from 'react';
import './app.scss';

import Main from '@/views/index/Main';
import Resize from '@/components/layout/Resize';
import useResize from '@/stores/useResize';
import ScrollSmoother from '@/lib/gsap/ScrollSmoother';
import useEventListener from '@/hooks/useEventListener';
import useBodyLock from '@/hooks/useBodyLock';
import Noice from '@/components/layout/noice/Noice';
import Preloader from '@/components/layout/preloader/Preloader';

function App() {
  const { isMobile } = useResize();
  const scrollSmootherRef = useRef(null);
  const wrapperRef = useRef(null);
  const contentRef = useRef(null);
  const bodyLockedRef = useRef(false);
  const { lockScroll, unlockScroll } = useBodyLock();
  const pageRef = useRef(null);

  const [showPreloader, setShowPreloader] = useState(true);

  useEffect(() => {
    lockScroll(document.documentElement);
    bodyLockedRef.current = true;

    console.log(isMobile);

    if (isMobile) return;

    scrollSmootherRef.current = ScrollSmoother.create({
      wrapper: wrapperRef.current,
      content: contentRef.current,
      smooth: 2,
      effects: true,
    });

    scrollSmootherRef.current.scrollTo(1, false);
  }, [isMobile]);

  const blockWheelRef = useRef(false);
  useEventListener(
    'wheel',
    (event) => {
      if (isMobile || bodyLockedRef.current) return;

      event.preventDefault();

      if (!blockWheelRef.current) {
        scrollSmootherRef.current.scrollTo(window.scrollY + event.deltaY, true);
        blockWheelRef.current = true;

        requestAnimationFrame(() => {
          blockWheelRef.current = false;
        });
      }
    },
    [isMobile],
    { listenerParams: { passive: false } },
  );

  const onPreloaderComplete = () => {
    unlockScroll(document.documentElement);
    bodyLockedRef.current = false;
    setShowPreloader(false);
  };

  const onPreloaderLoaded = () => {
    pageRef.current.playAnimation();
  };

  return (
    <Resize className="app">
      {() => (
        <>
          <Noice className="app__full-screen app__full-screen--noice" />
          {showPreloader && (
            <Preloader
              onLoaded={onPreloaderLoaded}
              onComplete={onPreloaderComplete}
              className="app__full-screen app__full-screen--preloader"
            />
          )}
          <div ref={wrapperRef}>
            <div ref={contentRef}>
              <Main ref={pageRef} className="app__content" />
            </div>
          </div>
        </>
      )}
    </Resize>
  );
}

export default App;
