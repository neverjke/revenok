import { defineConfig, loadEnv, normalizePath } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';
import svgr from 'vite-plugin-svgr';
import appConfig from './src/config/appConfig';

export default ({ mode }) => {
  process.env = {
    ...process.env,
    ...loadEnv(mode, process.cwd(), appConfig.ENV_PREFIX),
  };

  return defineConfig({
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData:
            '@import "@/styles/variables.scss";' +
            '@import "@/styles/mixins.scss";',
        },
      },
    },
    envPrefix: appConfig.ENV_PREFIX,
    plugins: [react(), svgr()],
  });
};
